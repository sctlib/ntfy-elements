// vite.config.js
import { resolve } from "path";
import { defineConfig } from "vite";
import { existsSync } from "fs";

export default defineConfig({
	base: "./",
	build: {
		lib: {
			entry: resolve(__dirname, "src/index.js"),
			formats: ["es"],
			name: "ntfy-elements",
			fileName: "ntfy-elements",
		},
		rollupOptions: {
			output: {
				dir: "dist-lib",
			},
		},
	},
});
