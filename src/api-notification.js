/* to communicate with the browser notifications API */
class Notifier {
	permission = Notification?.permission;

	/* request native notification permissions */
	async requestNotificationPermission() {
		if (!("Notification" in window)) return;
		let permission;
		try {
			permission = await Notification.requestPermission();
		} catch (error) {}
		return permission;
	}

	/* Since not rendering the notifications as messages in the DOM,
		 we notify the user, of new a message, with an alternative(s) notifier
		 notification = { id: "OCSeEQ93jtym", time: 1668677669, event: "message", topic: "__ntfy-elements", message: "triggered" }
	 */
	async notify(notification, topic = "", hostname = "") {
		let permission = await this.requestNotificationPermission();
		if (permission === "granted") {
			this._notifyClientNative(notification);
		} else {
			this._notifyClientLog(notification);
		}
	}

	/* check if the navigator has granted
		 permissions to a specific browser apim here 'notifications';
		 can be used with `permission.onchange`
	 */
	async navigatorPermission() {
		let permission;
		if ("permissions" in navigator) {
			permission = await navigator.permissions.query({
				name: "notifications",
			});
			return permission;
		}
	}

	/* use native browser/os/client notification system */
	_notifyClientNative({ message }) {
		const options = {};
		new Notification(message, options);
	}

	/* log to the console aa fallback notification,
		 when native not supported/allowed */
	_notifyClientLog({ message }, title) {
		console.info(`${message}`, "@", title);
	}
}
export { Notifier };

/* export a default instance */
export default new Notifier();
