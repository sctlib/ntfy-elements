import { LitElement, html } from "lit";
import { NtfyApi } from "./api.js";
import { formatTimestamp } from "./utils.js";

/*
	 `subscribe` to events from a `topic` for `message`(s)
	 - https://docs.ntfy.sh/subscribe/api/
 */
export default class NtfyPublish extends LitElement {
	static get properties() {
		return {
			topic: { type: String },
			server: { type: String },
			ntfyId: { type: String, reflect: true, attribute: "ntfy-id" },
			openTime: { type: String, reflect: true, attribute: "open-time" },
			messages: { type: Array },
			native: { type: Boolean },
			errorSubscribe: { type: Object },
			nativeNotifPermission: { type: Object, state: true },
		};
	}

	get loading() {
		return !this.topic || !this.ntfyId || !this.openTime;
	}

	get topicHost() {
		return this.api.getTopicHost(this.topic);
	}

	constructor() {
		super();
		/* updated by attribute */
		this.topic = "";
		this.server = "";
		this.native = false;
		/* updated by _onMessage */
		this.messages = [];
		/* updated by _onOpen */
		this.open = false;
		this.openTime = "";
		this.ntfyId = "";
		/* updated by _onError */
		this.errorSubscribe = null;
		/* updated by property */
		this.api = null;
	}

	/* when the topic changes, usbscribe to the new topic in a clean state */
	attributeChangedCallback(name, oldVal, newVal) {
		super.attributeChangedCallback(...arguments);
		if (["topic", "server"].indexOf(name) > -1) {
			this._reset();
			this._initApi();
			this._unsubscribeTopic();
			this._subscribeTopic();
		}
	}

	async connectedCallback() {
		super.connectedCallback();
		this.nativeNotifPermission = await this.api.notifier.navigatorPermission();
		if (this.nativeNotifPermission) {
			this.nativeNotifPermission.onchange =
				this._onNotificationPermissionChange.bind(this);
		}
	}

	disconnectedCallback() {
		super.disconnectedCallback();
		this._unsubscribeTopic();
	}

	_initApi() {
		this.api = new NtfyApi({ serverUrl: this.server });
	}

	/* reset topic subscription properties and values */
	_reset() {
		this.messages = [];
		this.open = false;
		this.openTime = "";
		this.ntfyId = "";
	}

	/* subscribe to a topic's event listeners */
	_subscribeTopic() {
		if (!this.topic) return;
		this.eventSource = this.api.subscribe({ topic: this.topic });
		if (this.eventSource) {
			this.eventSource.addEventListener("error", this._onError.bind(this));
			this.eventSource.addEventListener("open", this._onOpen.bind(this));
			this.eventSource.addEventListener("message", this._onMessage.bind(this));
		}
	}

	/* unsusbscribe to topic events */
	_unsubscribeTopic() {
		if (this.eventSource) {
			this.eventSource.close();
			this.eventSource.removeEventListener("message", this._onMessage);
			this.eventSource.removeEventListener("open", this._onOpen);
			this.eventSource.removeEventListener("error", this._onError);
			this.eventSource = null;
		}
	}

	render() {
		// use native browser/os notification center to render
		if (this.native) {
			return [this._renderHeader(), this._renderNative()];
		} else if (this.loading) {
			return this._renderSubscribing();
		} else if (this.messages && this.messages.length) {
			// if not native render messages in DOM
			return [this._renderHeader(), this._renderMessages()];
		} else {
			return [this._renderHeader(), this._renderNoMessages()];
		}
	}
	_renderHeader() {
		return html`
			<header part="header">
				<label part="topic">${this.topicHost}</label>
				${this.ntfyId ? this._renderHeaderOpen() : ""}
			</header>
		`;
	}
	_renderNative() {
		return html`
			<aside part="native">
				Listening for notifications... ${this._renderAuthorize()}
				${this.nativeNotifPermission ? "(using native display)" : null}
			</aside>
		`;
	}
	_renderAuthorize() {
		switch (Notification.permission) {
			case "denied":
				return html`
					<i>
						(browser notifications denied by user, logging messages into the
						console)
					</i>
				`;
			case "default":
				return html`
					<i>
						a
						<a
							target="_blank"
							rel="noorigin noopener"
							href=${this.api._buildTopicTriggerEndpoint(this.topic)}
						>
							first notification
						</a>
						incoming will request access to the browser notification permissions
					</i>
				`;
			case "granted":
				break;
			default:
				break;
		}
	}
	_renderHeaderOpen() {
		return html`
			<address part="id">${this.ntfyId}</address>
			<time part="open-time"> ${formatTimestamp(this.openTime)} </time>
		`;
	}

	/* trying out `progress`; but not so many steps to load */
	_renderSubscribing() {
		let value = 0;
		if (this.topic) {
			value = 25;
		} else if (this.ntfyId) {
			value = 50;
		} else if (this.openTime) {
			value = 100;
		}

		if (this.errorSubscribe) {
			return html`Error subscribing to
			${this.api._buildTopicEndpoint(this.topic)}`;
		} else {
			return html` <progress value=${value} max="100"></progress> `;
		}
	}

	_renderMessages() {
		return html`
			<output>
				<ul part="messages">
					${this.messages.map(this._renderMessage.bind(this))}
				</ul>
			</output>
		`;
	}
	_renderMessage(message) {
		return html`
			<li>
				<ntfy-message
					.notification=${message}
					exportparts=${this._buildMessageExportParts()}
				></ntfy-message>
			</li>
		`;
	}
	_renderNoMessages() {
		return html`Waiting for messages, none received yet`;
	}

	/* Subscribtion event source errors*/
	_onError(event) {
		this.errorSubscribe = event;
	}

	/* Subscription channel is open
		 {event: "open", id: "umhj6HxHwilS", time: 1668676361, topic: "__ntfy-elements" } */
	_onOpen(event) {
		console.log("_onOpen", event.data, event);
		if (event.type === "open") {
			this.open = true;
		}
		if (event.data) {
			const data = JSON.parse(event.data);
			this.ntfyId = data.id;
			this.openTime = data.time;
		}
	}

	/* Incoming channel message (notification) from the subscribed topic
		 { id: "OCSeEQ93jtym", time: 1668677669, event: "message", topic: "__ntfy-elements", message: "triggered" }
	 */
	_onMessage(event) {
		if (!event.type === "message" || !event.data) return;
		const notification = JSON.parse(event.data);

		// adding to the array will trigger a re-render of the DOM
		this.messages = [notification, ...this.messages];

		// if we use native notification rendering
		if (this.native) {
			this.api.nativeNotify(notification, this.topic);
		}
	}

	_onNotificationPermissionChange() {
		this.requestUpdate();
	}

	/* Utility: to build the value of exportparts="" attribute for ntfy-message
		 `message: message, header: message-header, time: message-time, id: message-id, body: message-body` because used in the shadowm DOM */
	_buildMessageExportParts() {
		const messageParts = {
			message: "message",
			header: "message-header",
			time: "message-time",
			id: "message-id",
			body: "message-body",
			title: "message-title",
			actions: "message-actions",
			action: "message-action",
			attachment: "message-attachment",
			"attachment-url": "message-attachment-url",
			"attachment-name": "message-attachment-name",
		};
		return Object.entries(messageParts)
			.map(([childrenPart, exportedPart]) => {
				return `${childrenPart}: ${exportedPart}`;
			})
			.reduce((acc, value) => {
				return `${acc}, ${value}`;
			});
	}
}
