import { LitElement, html } from "lit";
import { NtfyApi } from "./api.js";
import pkgJson from "../package.json";

export default class NtfyPublish extends LitElement {
	static get properties() {
		return {
			topic: { type: String, reflect: true },
			server: { type: String, reflect: true },
			trigger: { type: Boolean },
			native: { type: Boolean },
			errorPublish: { type: Object },
			serverAvailable: { type: Boolean },
		};
	}

	get topicHost() {
		return this.api.getTopicHost(this.topic);
	}

	attributeChangedCallback(name) {
		super.attributeChangedCallback(...arguments);
		if (["topic", "server"].indexOf(name) > -1) {
			this._initApi();
		}
	}

	constructor() {
		super();
		/* updated by attribute */
		this.topic = "";
		this.server = "";
		this.trigger = false;
		this.native = false;
		/* updated by property */
		this.api = null;
		this.serverAvailable = true; // let's imagine by default it is
		this._initApi();
	}

	async connectedCallback() {
		super.connectedCallback();
		/* check if the requested server/topic is available;
		 if trigger requested, use that as response */
		if (this.trigger) {
			this.serverAvailable = this.api.trigger({ topic: this.topic });
		} else {
			this.serverAvailable = await this.api.ping({ topic: this.topic });
		}
	}

	_initApi() {
		this.api = new NtfyApi({ serverUrl: this.server });
	}

	/* when the form is submitted, publish its content as a notification */
	async _onSubmit(formSubmitEvent) {
		formSubmitEvent.preventDefault();
		const { target: $form } = formSubmitEvent;

		const { body } = this._getFormData($form, ["body"]);
		this._disableForm($form);
		let publishRes;
		try {
			if (this.native) {
				let notification;
				if (typeof body === "object") {
					notification = body;
				} else {
					notification = { message: body };
				}
				publishRes = await this.api.nativeNotify(notification, this.topic);
			} else {
				publishRes = await this.api.publish({
					topic: this.topic,
					body,
				});
			}
		} catch (error) {
			console.warn("Error publishing message as NTFY notification", error);
			if (error) {
				this.errorPublish = error;
			} else {
				this.errorPublish = {
					error: `Error publishing ${this.topic} on ${this.api.serverUrl}`,
					link: pkgJson?.homepage,
					http: 404,
					code: 404001,
				};
			}
		}
		this._disableForm($form, false);

		if (publishRes && publishRes.id) {
			if (publishRes.topic !== this.topic) {
				this.errorPublish = {
					error: `Success, but there has been a conflict between requested topic (${this.topic}), and publication topic ${publishRes.topic}`,
					link: pkgJson?.homepage,
					http: 200,
					code: 201409,
				};
			} else {
				this.errorPublish = null;
				$form.reset();
			}
		}
	}

	_getFormData($form, fieldNames) {
		const formData = new FormData($form);
		const finalData = {};
		fieldNames.forEach((fieldName) => {
			const fieldValue = formData.get(fieldName);
			if (fieldName === "body") {
				let jsonData;
				try {
					jsonData = JSON.parse(fieldValue);
					finalData["body"] = jsonData;
				} catch (e) {
					finalData["body"] = fieldValue;
				}
			} else {
				finalData[fieldName] = fieldValue;
			}
		});
		return finalData;
	}

	_disableForm($form, disabled = true) {
		$form.querySelectorAll("fieldset").forEach(($fieldset) => {
			if (disabled) {
				$fieldset.setAttribute("disabled", disabled);
			} else {
				$fieldset.removeAttribute("disabled");
			}
		});
	}

	render() {
		if (this.serverAvailable || this.native) {
			return this._renderPublish();
		} else {
			return this._renderLoading();
		}
	}
	_renderPublish() {
		return html`
			<header part="header">
				<label part="topic">${this.topicHost}</label>
			</header>
			<form @submit=${this._onSubmit} part="form">
				<fieldset part="fieldset">
					<textarea
						name="body"
						part="body"
						placeholder=${`Publish new message to ${this.topicHost}`}
					></textarea>
					${this.errorPublish ? this._renderErrorPublish() : null}
				</fieldset>
				<fieldset part="fieldset">
					<button type="submit" part="submit">publish</button>
				</fieldset>
			</form>
		`;
	}

	_renderErrorPublish() {
		const { code, error, http, link } = this.errorPublish;
		return html`
			<output part="error">
				Error [ ${http} -
				<a href=${link} target="_blank" rel="noopener noreferrer">${code}</a>
				]
				<span>${error}</span>
			</output>
		`;
	}

	_renderLoading() {
		let val;
		if (this.topic && this.serverAvailable) {
			return html`<progress value=${50} max="100"></progress>`;
		} else if (!this.serverAvailable) {
			return html`Server ${this.api._buildTopicEndpoint(this.topic)} not
			available`;
		}
	}
}
