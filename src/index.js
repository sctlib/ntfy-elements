import NtfyPublish from "./ntfy-publish.js";
import NtfySubscribe from "./ntfy-susbscribe.js";
import NtfyMessage from "./ntfy-message.js";
import NtfyButton from "./ntfy-button.js";

import ntfyApi, { NtfyApi, NTFY_DEFAULT_SERVER_URL } from "./api.js";
import notifier, { Notifier } from "./api-notification.js";

window.customElements.define("ntfy-publish", NtfyPublish);
window.customElements.define("ntfy-subscribe", NtfySubscribe);
window.customElements.define("ntfy-message", NtfyMessage);
window.customElements.define("ntfy-button", NtfyButton);

export default ntfyApi;
export {
	NTFY_DEFAULT_SERVER_URL,
	NtfyApi,
	NtfySubscribe,
	NtfyPublish,
	NtfyMessage,
	NtfyButton,
	Notifier,
	ntfyApi as api,
	notifier,
};
