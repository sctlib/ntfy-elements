import { LitElement, html } from 'lit'
import { formatTimestamp } from './utils.js'

/* Display a notification's data as a DOM message */
export default class NtfyMessage extends LitElement {
	static get properties() {
		return {
			notification: { type: Object },
			message: { type: String },
			priority: { type: Number, reflect: true },
			id: { type: String, reflect: true },
			body: { reflect: true },
			time: { type: String, reflect: true },
			title: { type: String, reflect: true },
			actions: { type: Array, reflect: true },
		}
	}

	constructor() {
		super()
		/* set by property */
		this.notification = {}

		/* set from this.message */
		this.priority = 2
		this.id = ''
		this.time = ''
		this.message = null
		this.title = ''
		this.actions = []
	}

	updated() {
		super.updated()
		this._setProperties(this.notification)
	}

	_setProperties(message) {
		Object.keys(message).forEach(messageKey => {
			this[messageKey] = message[messageKey]
		})
	}

	render() {
		return html`
			<article part="message">
				<header part="header">
					${this.title ? this._renderTitle(this.title) : null}
					<address part="id">${this.id}</address>
					<time part="time">${formatTimestamp(this.time)}</time>
				</header>
				<blockquote part="body">${this.message}</blockquote>
				${this.attachment ? this._renderAttachment(this.attachment) : null}
				${this.actions.length ? this._renderActions(this.actions) : null}
			</article>
		`
	}
	_renderTitle(title) {
		return html`<h2 part="title">${title}</h2>`
	}
	_renderActions(actions) {
		return html`
			<menu part="actions">
				${actions.map(this._renderAction)}
			</menu>
		`
	}
	_renderAction(action) {
		return html`
			<li>
				<a
					target="_blank"
					rel="noreferrer noopener"
					href=${action.url}
					part="action"
				>
					${action.action} ${action.label}
				</a>
			</li>
		`
	}
	_renderAttachment({ url, name }) {
		return html`
			<figure part="attachment">
				<img src=${url} part="attachment-url"/>
				${name ? html`<figcaption part="attachment-name">${name}</figcaption>` : null}
			</figure>
		`
	}
}
