import { LitElement, html } from "lit";
import { NtfyApi } from "./api.js";
import pkgJson from "../package.json";
import NtfyPublish from "./ntfy-publish.js";

export default class NtfyButton extends NtfyPublish {
	static get properties() {
		return {
			message: {},
		};
	}
	constructor() {
		super();
	}
	async connectedCallback() {
		super.connectedCallback();

		if (this.message) {
			try {
				const jsonMessage = JSON.parse(this.message);
				this.message = jsonMessage;
			} catch (e) {
				// noop: this is not json!
			}
		}
	}
	/* when the form is submitted, publish its content as a notification */
	async _onSubmit(formSubmitEvent) {
		let publishRes;
		try {
			if (this.native) {
				publishRes = await this.api.nativeNotify(
					{ message: this.message },
					this.topic
				);
			} else {
				publishRes = await this.api.publish({
					topic: this.topic,
					body: this.message,
				});
			}
		} catch (error) {
			console.warn(
				`Error publishing message as ${this.native ? "" : "NTFY"} notification`,
				error
			);
			if (error) {
				this.errorPublish = error;
			} else {
				this.errorPublish = {
					error: `Error publishing ${this.topic} on ${this.api.serverUrl}`,
					link: pkgJson?.homepage,
					http: 404,
					code: 404001,
				};
			}
		}
	}
	_renderPublish() {
		return html`
			<button type="submit" part="submit" @click=${this._onSubmit}>
				publish to ${this.topicHost}
			</button>
		`;
	}
}
