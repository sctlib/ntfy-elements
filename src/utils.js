/* Transform `time/date` into human readable format (make it a date if not)  */
export function formatTimestamp(timestamp) {
	let date;

	if (typeof timestamp === "string") {
		date = new Date(timestamp);
	} else if (typeof timestamp === "number") {
		date = new Date(timestamp * 1000); // Multiply by 1000 to convert seconds to milliseconds
	} else {
		// Handle unsupported types
		return "Invalid timestamp";
	}

	if (!Number.isFinite(date.getTime())) {
		return "Invalid date";
	}

	return new Intl.DateTimeFormat("en", {
		dateStyle: "long",
		timeStyle: "short",
	}).format(date);
}
