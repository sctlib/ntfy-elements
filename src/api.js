import notifier from "./api-notification.js";

const NTFY_DEFAULT_SERVER_URL = "https://ntfy.sh";

/* API to communicate with a NTFY.sh server */
class NtfyApi {
	/* the defaults of the api */
	serverUrl = null;

	get hostname() {
		return new URL(this.serverUrl).hostname;
	}

	/* initialize the api with defaults */
	constructor({ serverUrl } = {}) {
		serverUrl = serverUrl || NTFY_DEFAULT_SERVER_URL;
		this.serverUrl = this._buildBaseURL(serverUrl);
		this.notifier = notifier;
	}

	/* check if the requested server is available */
	async ping({ topic = "" }) {
		const url = `${this._buildTopicEndpoint(topic)}/json?poll=1`;
		let pingRes;
		try {
			pingRes = await fetch(url);
		} catch (error) {
			console.warn("ping server error", error);
		}
		if (pingRes && pingRes.ok) {
			return true;
		} else {
			return false;
		}
	}

	/* publish a message to a NTFY endpoint */
	async publish({ topic = "", body = null } = {}) {
		if (!topic) throw "Missing topic";
		/* the publishing method to use */
		let publishTopicPromise;
		if (typeof body === "string") {
			publishTopicPromise = this._publishAsPOST.bind(this);
		} else if (typeof body === "object") {
			publishTopicPromise = this._publishAsJSON.bind(this);
		}

		let publishAnswer;
		try {
			const res = await publishTopicPromise(topic, body);
			publishAnswer = await res.json();
			if (publishAnswer.error) throw publishAnswer;
		} catch (e) {
			throw publishAnswer;
		}
		return publishAnswer;
	}

	/* default method to publish a message in a topic */
	_publishAsPOST(topic, body) {
		const config = { method: "POST", body };
		const endpoint = this._buildTopicEndpoint(topic);
		return fetch(endpoint, config);
	}

	/* method to publish a message in a topic as JSON object
		 https://docs.ntfy.sh/publish/#publish-as-json
	 */
	_publishAsJSON(topic, body) {
		body.topic = topic;
		const config = {
			method: "POST",
			body: JSON.stringify(body),
		};
		const endpoint = this.serverUrl;
		return fetch(endpoint, config);
	}

	/* Subscribe to a `topic` to get notifications via EventSource
		 eventSource.{onerror,onopen,onmessage}
	 */
	subscribe({ topic = "" } = {}) {
		if (!topic) throw "Missing topic";
		const endpoint = `${this._buildTopicEndpoint(topic)}/sse`;
		const eventSource = new EventSource(endpoint);
		return eventSource;
	}

	async trigger({ topic = "" }) {
		const url = this._buildTopicTriggerEndpoint(topic);
		let res;
		try {
			res = await fetch(url);
		} catch (error) {
			console.warn("Error triggering topic endpoint", error);
		}
		return res;
	}

	/* when we don't render to the DOM,
		 use a native browser/os notifier */
	nativeNotify(notification, topic) {
		this.notifier.notify(notification, topic, this.hostname);
	}

	getTopicHost(topic) {
		return `${topic}@${this.hostname}`;
	}

	/* be sure the base URL is https */
	_buildBaseURL(url) {
		const oldURL = new URL(url);
		let newURL = `https://${oldURL.hostname}${oldURL.pathname}`;
		if (newURL.endsWith("/")) {
			newURL = newURL.slice(0, newURL.length - 1);
		}
		return newURL;
	}

	/* make a URL of a topic */
	_buildTopicEndpoint(topic) {
		return `${this.serverUrl}/${topic}`;
	}
	_buildTopicTriggerEndpoint(topic) {
		return `${this._buildTopicEndpoint(topic)}/trigger`;
	}
}

/* export the API class,
	 so it is possible to create a new custom instance */
export default new NtfyApi();
export { NtfyApi, NTFY_DEFAULT_SERVER_URL };
