# ntfy-elements

Web-components to use push notifications with ntfy.sh services

# Web Components

By default, the components will use the ntfy server from `htps://ntfy.sh`.

- npm: [@sctlib/ntfy-elements](https://www.npmjs.com/package/@sctlib/ntfy-elements)

To start using the elements, reference the `@sctlib/ntfy-elements` on your site, from example from a CDN.

```html
<script src="https://cdn.jsdelivr.net/npm/@sctlib/ntfy-elements@0.0.4"></script>
```

> For styling, it is possible to use the CSS parts of each web
> components. Check the file `src/styles/index.css` for reference.

> Example usage: https://jsbin.com/xewemirufe/1/edit?html,output

## Usage

### With npm

- install `npm i @sctlib/ntfy-elements`

- import with this import, the components are already defined and registered as custom elemnts

```
import '@sctlib/ntfy-elements'
import ntfyApi from '@sctlib/ntfy-elements' ;; the default api instance
import * as ntfyElements '@sctlib/ntfy-elements'
```

## ntfy-publish

## `<ntfy-publish/>`

Component to publish a message to a ntfy server, so clients subscribed will get the message as notification.

### Usage

```html
<ntfy-publish topic="__ntfy-elements"></ntfy-publish>
```

### Attributes

- `topic`, under which topic do want to publish messages
- `server`, the server to be used for this topic, defaults to `https://ntfy.sh`
- `trigger`, set to `true` for the component to auto "trigger" on load

## `<ntfy-subscribe/>`

Component to subscribe to messages sent by a server.

### Usage

```html
<ntfy-subscribe topic="__ntfy-elements"></ntfy-subscribe>
```

- to stop subscribing: remove the element from the DOM
- to change subscribition topic, or server endpoint, remove the
  element from the DOM and create a new one with the updated
  attributes, or update the attributes

### Attributes

- `topic`, the topic to we want to receive messages for
- `server`, the server to be used for this topic, defaults to `https://ntfy.sh`
- `native` (`false`), should it render notifications in the native browser/operating-system notification server (Web Notification API),

# Alternative usage

Alternatively, this module can be embeded in any web page, and
imported from a CDN, or for example used with the demo site (forked)
in iframes, using URL search query parameters.

```html
<iframe src="https://sctlib.gitlab.io/ntfy-elements?topic=test-1234"></iframe>
<iframe src="https://sctlib.gitlab.io/ntfy-elements"></iframe>
```

# Docs

- https://docs.ntfy.sh
- https://unifiedpush.org
- https://github.com/binwiederhier/ntfy/blob/main/examples/web-example-eventsource/example-sse.html
- https://developer.mozilla.org/en-US/docs/Web/API/Notification/Notification
